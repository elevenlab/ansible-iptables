Ansible Role: Iptables
===================

Configure `iptables` on Debian servers.

It will add the rules defined in both `iptables` and `ip6tables`. It will also install `iptables-persistent` to ensure that these will survive over a reboot


----------

Role variables
-------------

Available variables are listed below, along with default values (see `defaults/main.yml`):

	iptables_allow_generated_tcp_traffic: yes
	iptables_allow_generated_udp_traffic: yes
These variables allow to shortcut the rules: `-A INPUT -p tcp/udp -m state --state ESTABLISHED,RELATED --dport 0:65535 -j ACCEPT` allowing traffic started from the machine to always pass throught the firewall, respectively for protocol `tcp` and `udp`

	iptables_allow_generated_tcp_traffic_interfaces: <not defined>
	iptables_allow_generated_udp_traffic_interfaces: <not defined>
These variables allow to specify a list of interfaces for which the previous rules apply. If not specified (i.e. by default) the rules are applied to *any* interface.

	iptables_input_policy: DROP
	iptables_output_policy: ACCEPT
	iptables_forward_policy: ACCEPT
Chains default Policy.

	iptables_rules: []
A list of rules to add to `iptables`. Based on policy you have set, you need to specify rules in this list in order to block or allow connection to the server.
Each rule ofthe list has it own defaults:
	
	- chain: INPUT
	  accepts: []
	  	 #if elements in the list are specified they have
		 - port: <any>
		   src: <any>
		   interface: <any>
		   protocol: <any>
		 #any is not a valid expression. It must be empty to actually be any 
      drops: []
		  #if elements in the list are specified they have the same variaibles as accepts

 An exemple list is also provided in `defaults/main.yml`:
 
	 iptables_exemple_rules:
	  - chain: INPUT
		accepts:
		  - port: 22
			protocol: tcp
		  - port: 53
			protocol: udp

---


All the above variable have their equivalent for `IPv6`
	
	ip6tables_*

Dependencies
-------------------
None.

Example Playbook
--------------------------
	- name: configure iptables
	  hosts: webservers
	  vars:
	    iptables_input_policy: DROP
		iptables_forward_policy: DROP
	  	ip6tables_input_policy: DROP
		ip6tables_forward_policy: DROP
		iptables_allow_generated_tcp_traffic: yes
	  	iptables_rules:
			- chain: INPUT
			  accepts:
			  	- interface: eth0
				- interface: eth1
				  protocol: tcp
				  port: 443
				  src: 10.0.0.0/24
	  roles:
		  - iptables